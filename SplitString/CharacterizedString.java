import java.util.*;

public class CharacterizedString{
    public static void main(String[] args) {
        Scanner inputString = new Scanner(System.in);
        System.out.print("Masukkan kata/kalimat, lalu tekan enter: ");
        String kataString = inputString.nextLine();

        char c = kataString.charAt(0);
        char[] c_arr = kataString.toCharArray();

        for (int i = 0; i < c_arr.length; i++){
            System.out.printf("Karakter[%d]: %c \n",i,c_arr[i]);
        }
    }
}